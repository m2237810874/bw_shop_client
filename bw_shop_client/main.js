import App from './App'
import goPage from './utils/utils'
// #ifndef VUE3
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui'
Vue.prototype.$cloud=newCloud()
import { createSSRApp } from 'vue'
Vue.config.productionTip = false
Vue.prototype.$goPage=goPage;

async function newCloud(){
  const n_clound=new wx.cloud.Cloud({
    resourceEnv:"min-gucof",
    traceUser:true
  });
  //初始化
  await n_clound.init();
  return n_clound;
}



App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'

export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif
Vue.use(uView)