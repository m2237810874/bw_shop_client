export const formatUrl=(url,query)=>{
    return Object.keys(query).length?(url+'?'+Object.keys(query).map(key=>`${key}=${query[key]}`).join('&')):url
}
export default function ({url='',query={},redirect=false,tabbar=false}){
    // 跳转页面是tabbar swicthTab
    // redirect  redirectTo
    // 默认使用navigateTo
    url=formatUrl(url,query)
    const typeName=redirect?'redirectTo':tabbar?'switchTab':'navigateTo'
    console.log(url);
    console.log(typeName);
    return new Promise((resolve,reject)=>{
        uni[typeName]({
            url,
            success(res){
                resolve(res)
            },
            fail(err){
                reject(err)
            }
        })
    })
}


//防抖
export function debounce(func, delay = 300, immediate = false) {
    let timer = null
    return function() {
        if (timer) {
            clearTimeout(timer)
        }
        if (immediate && !timer) {
            func.apply(this, arguments)
        }
        timer = setTimeout(() => {
         func.apply(this, arguments)
        }, delay)
    }
}

//节流
export function throttle(func, wait) {
    var timeout;
    var previous = 0;
    return function() {
        context = this;
        args = arguments;
        if (!timeout) {
            timeout = setTimeout(function() {
                timeout = null;
                func.apply(context, args)
            }, wait)
        }

    }
}