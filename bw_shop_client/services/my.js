import request from "../utils/http";
// 获取token
export const _login=(data)=>request.post('/login',data)
// 设置用户信息  用户授权之后拿到用户信息  拿到用户信息之后  保存用户信息
export const save_user_info=data=>request.put('/p/user/setUserInfo',data)
// 获取分类的商品左侧数据              /p/user/collection/prods?current=1&size=10
export const get_collect_List_length = () => request.get("/p/user/collection/count")
