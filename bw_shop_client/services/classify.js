import request from "../utils/http";
// 获取分类的商品左侧数据 
export const classifyLeft = (data) => request.get("/category/categoryInfo",data)
// 获取分类商品右侧数据 
export const classifyRight = id => request.get(`/prod/pageProd`,{categoryId:id})