import request from "../utils/http"


export const cityList=()=>request.get("/p/address/list")


//获取地址列表
export const addAddrList=(params)=>request.get("/p/addressst",params)
//保存地址
export const addAddrApi=(params)=>request.post("/p/address/addAddr",params)
//编辑地址回显
export const addAddrEdita=(id)=>request.get(`/p/address/addrInfo/${id}`)
//编辑地址
export const addAddrUpdate=(params)=>request.put(`/p/address/updateAddr`,params)
//默认地址
export const defaultAddr=(id)=>request.put(`/p/address/defaultAddr/${id}`)
//删除地址
export const addAddrDel=(id)=>request.delete(`/p/address/deleteAddr/${id}`)
export const set_defaultAddress=(id)=>request.put(`/p/address/defaultAddr/${id}`)
