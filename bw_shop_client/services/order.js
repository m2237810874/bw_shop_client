import request from "../utils/http";
export const get_order_detail=(data)=>request.post("/p/order/confirm",data);
export const submit_order_list=(data)=>request.post("/p/order/submit",data);
export const order_pay=(data)=>request.post('/p/order/pay',data)