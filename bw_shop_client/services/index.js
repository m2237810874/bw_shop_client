import request from "../utils/http";
//获取首页轮播图数据
export const get_swiper_img=()=>request.get('/indexImgs')
//获取公告
export const get_tag_list=()=>request.get('/shop/notice/topNoticeList')
// 获取标题
export const getTag=()=>request.get('/prod/tag/prodTagList')
//获取与标题相对于得内容
export const getTagProd=(data)=>request.get('/prod/prodListByTagId',data)
