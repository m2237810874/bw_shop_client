import request from "../utils/http";
export const get_orderList=(data)=>request.get('/p/myOrder/myOrder',data)
export const del_order=(orderNumber)=>request.put(`/p/myOrder/cancel/${orderNumber}`)
export const del_order_List=(orderNumber)=>request.delete(`/p/myOrder/${orderNumber}`)