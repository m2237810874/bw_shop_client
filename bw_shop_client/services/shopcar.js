import request from "../utils/http";
//购物车数据
export const shopCarlist=()=>request.post("/p/shopCart/info")

export const totalPay=(body)=>request.post('/p/shopCart/totalPay',body)

export const changeNum=(data)=>request.post('/p/shopCart/changeItem',data)

export const del_All=(data)=>request.delete('/p/shopCart/deleteItem',data)
