import request from "../utils/http";
// 根据商品id 获取该商品的详情数据

export const _get_detail_List=id=>request.get('/prod/prodInfo',{prodId:id})
export const add_collect_List=(data)=>request.post('/p/user/collection/addOrCancel',data)
export const flag_collect_List_id=id=>request.get('/p/user/collection/isCollection',{prodId:id})
export const add_shopcar_List=(data)=>request.post('/p/shopCart/changeItem',data)